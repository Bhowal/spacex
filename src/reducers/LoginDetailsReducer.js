const DefaultState={
    loading:false,
    data:{},
    errorMsg:"",

};

const LoginDetailsReducer = (state=DefaultState,action)=>{
    console.log(action.payload)
    switch (action.type) {
        case "DETAILS_LOADING":
            return{
                ...state,
                loading:true,
                errorMsg:""
            };
            
            case "DETAILS_FAIL":
            return{
                ...state,
                loading:false,
                errorMsg:"unable to get pokemon"
            };
            case "DETAILS_SUCCESS":
            return{
                
                ...state,
                loading:false,
               data:action.payload,
               errorMsg:""
            };
    
        default:
            return state;
    }
};
export default LoginDetailsReducer;