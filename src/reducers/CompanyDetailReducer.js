const DefaultState={
    loading:false,
    data:[],
    errorMsg:"",

};

const CompanyDetailReducer = (state=DefaultState,action)=>{
    console.log(action.payload)
    switch (action.type) {
        case "COMPANY_DETAILS_LOADING":
            return{
                ...state,
                loading:true,
                errorMsg:""
            };
            
            case "COMPANY_DETAILS_FAIL":
            return{
                ...state,
                loading:false,
                errorMsg:"unable to get DATA"
            };
            case "COMPANY_DETAILS_SUCCESS":
            return{
                
                ...state,
                loading:false,
               data:action.payload,
               errorMsg:""
            };
    
        default:
            return state;
    }
};
export default CompanyDetailReducer;