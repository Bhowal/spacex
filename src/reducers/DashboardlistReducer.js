const DefaultState={
    loading:false,
    data:[],
    errorMsg:"",
    getData:false

};

const DashboardlistReducer = (state=DefaultState,action)=>{
    console.log(action.payload)
    switch (action.type) {
        case "CAPSULES_DETAILS_LOADING":
            return{
                ...state,
                loading:true,
                errorMsg:""
            };
            
            case "CAPSULES_DETAILS_FAIL":
            return{
                ...state,
                loading:false,
                errorMsg:"unable to get DATA"
            };
            case "CAPSULES_DETAILS_SUCCESS":
            return{
                
                ...state,
                loading:false,
               data:action.payload,
               errorMsg:""
            };
    
        default:
            return state;
    }
};
export default DashboardlistReducer;