const DefaultState={
    loading:false,
    data:[],
    errorMsg:"",
    getData:false

};

const DashboardCapsuleDetailsReducer = (state=DefaultState,action)=>{
    console.log(action.payload)
    switch (action.type) {
        case "ONE_CAPSULES_DETAILS_LOADING":
            return{
                ...state,
                loading:true,
                errorMsg:""
            };
            
            case "ONE_CAPSULES_DETAILS_FAIL":
            return{
                ...state,
                loading:false,
                errorMsg:"unable to get DATA"
            };
            case "ONE_CAPSULES_DETAILS_SUCCESS":
            return{
                
                ...state,
                loading:false,
               data:action.payload,
               errorMsg:""
            };
    
        default:
            return state;
    }
};
export default DashboardCapsuleDetailsReducer;