import {combineReducers} from 'redux';
import LoginDetailsReducer from './LoginDetailsReducer';
import CompanyDetailReducer from './CompanyDetailReducer';
import DashboardlistReducer from './DashboardlistReducer';
import DashboardCapsuleDetailsReducer from './DashboardCapsuleDetailsReducer';

const RootReducer = combineReducers({
    LoginDetails:LoginDetailsReducer,
    CompanyDetail:CompanyDetailReducer,
    Dashboardlist:DashboardlistReducer,
    DashboardCapsuleDetails:DashboardCapsuleDetailsReducer

});

export default RootReducer;