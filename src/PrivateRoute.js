import React from 'react';
import {Route,Redirect} from 'react-router-dom';
import {isAuthenticate} from './Authentication';

const PrivateRoute = ({children, ...rest}) =>{
    const auth=isAuthenticate();
    return(
    <Route {...rest} render={()=>auth?(children):(<Redirect to={'/'}/>)} />
    )
}
export default PrivateRoute;