import axios from 'axios';

export  const GetCompanyDetails = () => async dispatch =>{
    // console.log(page)
    try{
        dispatch({
            type:"COMPANY_DETAILS_LOADING"
        });
        // const perPage=15;
        // const offset=(page*perPage) - perPage;
        const res = await axios.get("https://api.spacexdata.com/v3/info")

        dispatch({
            type:"COMPANY_DETAILS_SUCCESS",
            payload:res.data
        })
    }catch(e){
        dispatch({
            type:"COMPANY_DETAILS_FAIL"
        })
    }
};