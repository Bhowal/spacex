import axios from 'axios';

export  const GetCapsulesDetails = (serialId) => async dispatch =>{
    // console.log(page)
    try{
        dispatch({
            type:"ONE_CAPSULES_DETAILS_LOADING"
        });
        // const perPage=15;
        // const offset=(page*perPage) - perPage;
        const res = await axios.get(`https://api.spacexdata.com/v3/capsules/${serialId}`)

        dispatch({
            type:"ONE_CAPSULES_DETAILS_SUCCESS",
            payload:res.data
        })
    }catch(e){
        dispatch({
            type:"ONE_CAPSULES_DETAILS_FAIL"
        })
    }
};