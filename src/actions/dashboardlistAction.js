import axios from 'axios';

export  const GetAllCapsules = () => async dispatch =>{
    // console.log(page)
    try{
        dispatch({
            type:"CAPSULES_DETAILS_LOADING"
        });
        // const perPage=15;
        // const offset=(page*perPage) - perPage;
        const res = await axios.get("https://api.spacexdata.com/v3/capsules")

        await dispatch({
            type:"CAPSULES_DETAILS_SUCCESS",
            payload:res.data
        })
    }catch(e){
        dispatch({
            type:"CAPSULES_DETAILS_FAIL"
        })
    }
};