import axios from 'axios';

export  const GetLoginDetails = () => async dispatch =>{
    // console.log(page)
    try{
        dispatch({
            type:"DETAILS_LOADING"
        });
        // const perPage=15;
        // const offset=(page*perPage) - perPage;
        const res = await axios.get("https://run.mocky.io/v3/fc7da6ed-7cf0-43bb-a667-c69d410e80de")

        dispatch({
            type:"DETAILS_SUCCESS",
            payload:res.data
        })
    }catch(e){
        dispatch({
            type:"DETAILS_FAIL"
        })
    }
};