
import { Switch, Route, NavLink, Redirect } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/login';
import Dashboard from './pages/Dashboard';
import PrivateRoute from './PrivateRoute';

function Routes() {
  return (
    <div>



      <Switch>
        <Route path={"/"} exact component={Home} />
        <Route path={"/login"} exact component={Login} />
        <PrivateRoute path={"/dashboard"}>
          <Dashboard />
        </PrivateRoute>
        <Redirect to={"/"} />
      </Switch>
    </div>
  );
}

export default Routes;
