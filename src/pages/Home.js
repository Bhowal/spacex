

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GetCompanyDetails } from '../actions/companyAction';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { isAuthenticate, logout } from '../Authentication'


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));
const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        SARANI BHOWAL
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const Home = (props) => {
  const dispatch = useDispatch();
  const companyDet = useSelector(state => state.CompanyDetail);
  console.log(props)
  const classes = useStyles();
  const [company, setCompany] = useState([]);
  useEffect(() => {
    fetchHomePageDetails();
  }, [])

  const fetchHomePageDetails = () => {
    dispatch(GetCompanyDetails())
    setCompany(companyDet.data)
    console.log(companyDet.data)
  }
  const logOut = () => {
    localStorage.removeItem('userDetails');
    window.location.reload();
  }

  return (

    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          
          {props.pathname === undefined &&
            <Typography variant="h6" className={classes.title}>
              SpaceX
            </Typography>
          }
          {isAuthenticate() &&
          
            <Typography variant="h6" className={classes.title}>
              <Button style={{ color:'#fff'}} onClick={()=>props.history.push('/dashboard')}>
              Dashboard
              </Button>
            </Typography>
          }


          



          {isAuthenticate() ?
            <Button color="inherit" onClick={logOut}>Logout</Button>
            :

            <Button color="inherit" onClick={() => props.history.push('/login')}>Login</Button>
          }


        </Toolbar>
      </AppBar>
      {console.log(companyDet.data)}
      {company && 
      <React.Fragment>

      <main>
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              {companyDet.data.name}
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              {companyDet.data.summary}.
          </Typography>


            <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>Our Website-
    <Link href='https://www.spacex.com/'>
                      www.spacex.com
    </Link>
                  </Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>Elon twitter-
    <Link href='https://twitter.com/elonmusk'>
    https://twitter.com/elonmusk
    </Link>
                  </Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>Our Twitter-
    <Link href='https://twitter.com/SpaceX'>
    https://twitter.com/SpaceX
    </Link>
                  </Typography>
                </Grid>
              </Grid>
              
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>CEO-
                    {companyDet.data.ceo}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>COO-
                    {companyDet.data.coo}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>CTO-
                    {companyDet.data.cto}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>Emplyees-
                    {companyDet.data.employees}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>Founded-
                    {companyDet.data.founded}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Typography className={classes.root}>Founder-
                    {companyDet.data.founder}
                  </Typography>
                </Grid>
              </Grid>
              
            </div>
            
          </Container>
        </div>
        
        
      </main>
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          Contact Us
      </Typography>
        <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          Address- Rocket Road
      </Typography>
      <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          City- Hawthorne
      </Typography>
      <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          State- California
      </Typography>
        <Copyright />
      </footer>
    </React.Fragment>
 
      }
       </div>

  );
}

export default Home;
