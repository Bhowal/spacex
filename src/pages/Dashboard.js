import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllCapsules } from '../actions/dashboardlistAction';
import { GetCapsulesDetails } from '../actions/capsuledetailsAction';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import { isEmpty } from 'lodash';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  rootCard: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));
const Dashboard = (props) => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const capsuleDet = useSelector(state => state.Dashboardlist);
  const [capsules, setCapsules] = useState([]);

  const [capsuleDetails, setCapsuleDetails] = useState([]);
  const oneCapsuleDet = useSelector(state => state.DashboardCapsuleDetails);

  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    fetchHomePageDetails();
  }, [])
  // capsuleDet.loading

  const fetchHomePageDetails = () => {
    dispatch(GetAllCapsules())
    setCapsules(capsuleDet.data)
    console.log(capsuleDet)
  }

  const capsulesDetails = (capsuleId) => {
    dispatch(GetCapsulesDetails(capsuleId))
    setCapsuleDetails(oneCapsuleDet.data)
    console.log(oneCapsuleDet)
  }
  const logOut = () => {
    localStorage.removeItem('userDetails');
    window.location.reload();
  }

  const CapsuleDetails = () => {
    if (!isEmpty(oneCapsuleDet.data)) {
      return (
        <Grid item xs={6}>
          {console.log(oneCapsuleDet.data)}
          <Paper className={classes.paper} style={{ textAlign: '-webkit-center' }}>
            <Card className={classes.rootCard}>
              <CardHeader

                title={oneCapsuleDet.data.capsule_id + ' - ' + oneCapsuleDet.data.capsule_serial}
                subheader={oneCapsuleDet.data.original_launch}
              />
              <CardMedia
                className={classes.media}
                image="https://g.foolcdn.com/editorial/images/454259/falcon-9.jpg"
                title="Paella dish"
              />
              <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                  {oneCapsuleDet.data.details}
                </Typography>
              </CardContent>
              <CardActions disableSpacing>

                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                  aria-label="show more"
                >
                  <ExpandMoreIcon />
                </IconButton>
              </CardActions>
              <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                  <Typography paragraph>Method:</Typography>
                  <Typography paragraph>
                    This article is about the rocket manufacturer. For the British art gallery, see Spacex (art gallery).
    "Space Exploration Technologies" redirects here. For the general topics, see Space exploration and Space technology.
                </Typography>
                  <Typography paragraph>
                    Space Exploration Technologies Corp. (SpaceX) is an American aerospace manufacturer and
                     space transportation services company headquartered in Hawthorne, California. It was founded
                     in 2002 by Elon Musk with the goal of reducing space transportation costs to enable the colonization of Mars.
                     SpaceX manufactures the Falcon 9 and Falcon Heavy launch vehicles,
                     several rocket engines, Dragon cargo and crew spacecraft and Starlink satellites.
                </Typography>
                  <Typography paragraph>
                    SpaceX is developing a large internet satellite constellation named Starlink. In January 2020
                     the Starlink constellation became the largest satellite constellation in the world. SpaceX is
                     also developing Starship, a privately funded super heavy-lift launch system for interplanetary
                     spaceflight. Starship is intended to become the primary SpaceX orbital vehicle once operational,
                      supplanting the existing Falcon 9, Falcon Heavy and Dragon fleet. Starship will be fully reusable
                      and will have the highest payload capacity of any orbital rocket ever on its debut, scheduled for
                      the early 2020s.
                </Typography>

                </CardContent>
              </Collapse>
            </Card>

          </Paper>




        </Grid>
      )
    }
    else {
      return (<Typography component="h2" style={{ padding: '30px' }}>
        Click on the list to show details
    </Typography>)
    }
  }

  const ShowDataCapsulesList = () => {
    if (!isEmpty(capsuleDet.data)) {
      return (
        <div className={classes.root} >
          <Grid container spacing={3}>

            <Grid item xs={6}>
              <Paper className={classes.paper}>
                <List className={classes.root}>
                  {capsuleDet.data.length > 0 && capsuleDet.data.map((capsule, key) => {
                    return (
                      <div key={key}>
                        <ListItem alignItems="flex-start" onClick={() => capsulesDetails(capsule.capsule_serial)}>
                          <ListItemAvatar>
                          </ListItemAvatar>
                          <ListItemText
                            primary={capsule.capsule_id + '-' + capsule.capsule_serial}
                            secondary={
                              <React.Fragment>
                                <Typography
                                  component="span"
                                  variant="body2"
                                  className={classes.inline}
                                  color="textPrimary"
                                >
                                  {capsule.details}
                                </Typography>
                              </React.Fragment>
                            }
                          />
                        </ListItem>
                        <Divider variant="inset" component="li" />
                      </div>
                    )
                  })}

                </List>
              </Paper>
            </Grid>

            <CapsuleDetails />


          </Grid>
        </div>

      )
    }
    else {
      return <p>unable..........</p>
    }
  }
  console.log(capsules.length)
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>

          <Typography variant="h6" className={classes.title}>
            Dashboard
            </Typography>
          <Button color="inherit" onClick={() => props.history.push('/')}>Back to Home Page</Button>



        </Toolbar>
      </AppBar>
      <ShowDataCapsulesList />

    </div>
  )
}
export default withRouter(Dashboard);