import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Home from './Home'; 
import {GetLoginDetails} from '../actions/loginAction';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit">
             SARANI BHOWAL
        </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));
const Login = (props) => {
    const dispatch = useDispatch();
    const userDet= useSelector(state=>state.LoginDetails);
    console.log(props)
    const classes = useStyles();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(false);
    const [errMsg, setErrMsg] = useState('');
    React.useEffect(() => {
        // let page = 3;
        FetchData();
    }, [])
    const FetchData = () => {
        dispatch(GetLoginDetails())
    }
    // console.log(userDet.data)

    const loginData =userDet.data
    const loggedInDetails =userDet.data
    console.log(loginData.name)
    const signIn = () => {
        // alert('login')
        // console.log(username,password)
        if (username == '' || password == '') {
            setError(true);
            setErrMsg('All fields must not be empty')
        }
        else {
            if (username == loginData.name && password == loginData.password) {
                // alert('loggedin')
                localStorage.setItem('userDetails', JSON.stringify(loggedInDetails))
                props.history.push('/dashboard')
            }
            else {
                setError(true);
                setErrMsg('Username and Password does not match. Please try again!')
            }
        }

    }

    const handleChange = prop => event => {
        setErrMsg('')
        if (prop == 'username') {
            console.log(event.target.value)
            setUsername(event.target.value)
        }
        if (prop == 'password') {
            console.log(event.target.value)
            setPassword(event.target.value)
        }
    }
    //https://run.mocky.io/v3/c0ec9d23-7345-4843-a50b-c13c486306aa
    return (
        <div className={classes.root}>
            {/* <Home pathname={props.location.pathname}/> */}
            <AppBar position="static">
                <Toolbar>

                    <Typography variant="h6" className={classes.title}>
                        Login
            </Typography>
                    <Button color="inherit" onClick={() => props.history.push('/')}>Back to Home Page</Button>



                </Toolbar>
            </AppBar>

            <Container component="main" maxWidth="xs">

                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
        </Typography>
                    <form className={classes.form} noValidate>
                        {/* <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={() => props.history.push('/')}
                        >
                            Back to Dashboard
                    </Button> */}
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="username"
                            name="username"
                            autoComplete="username"
                            autoFocus
                            onChange={handleChange('username')}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={handleChange('password')}
                        />
                        {/* <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                    /> */}
                        {error &&
                            <div style={{ color: 'red' }}>{errMsg}</div>
                        }

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={signIn}
                        >
                            Sign In
                    </Button>

                        {/* <Grid container>
                        <Grid item xs>
                            <Link href="#" variant="body2">
                                Forgot password?
              </Link>
                        </Grid>
                        <Grid item>
                            <Link href="#" variant="body2">
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid> */}
                    </form>
                </div>
                <Box mt={8}>
                    <Copyright />
                </Box>
            </Container>
        </div>
    )
}
export default Login;